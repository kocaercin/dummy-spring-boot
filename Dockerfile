FROM maven:3.5.2-jdk-8-alpine
WORKDIR /app
COPY . /app
RUN mvn package
CMD ["java","-jar","target/helloworld-0.0.1-SNAPSHOT.jar"]